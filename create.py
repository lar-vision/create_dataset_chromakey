
import argparse
from lib.create_dataset import CKDataset

''' Find the masks of the images contained in @inputfolder and replace the backgrounds with those from @backgroundfolder, then save them in @outputfolder.
Set the HSV levels @ck_low_lv and @ck_low_lv if known, otherwise leave the argument empty for finding them. '''

parser = argparse.ArgumentParser()
parser.add_argument("--backgroundfolder", type=str, default="/media/riccardo/data1/datasets/dlo_segmentation/data_chromakey/bg")
parser.add_argument("--inputfolder", type=str, default="/media/riccardo/data1/datasets/dlo_segmentation/data_chromakey/PAPER/tmp")
parser.add_argument("--outputfolder", type=str, default="/media/riccardo/data1/datasets/dlo_segmentation/data_chromakey/ck_results/tmp")
parser.add_argument("--lastindex", type=int, default=-1)
parser.add_argument("--augment_background", type=bool, default=True)
parser.add_argument("--augment_foreground", type=bool, default=True)
parser.add_argument("--output_shape", type=tuple, default=(-1, -1))
parser.add_argument("--debug", type=bool, default=True)
parser.add_argument("--clean_folders", type=bool, default=True)
parser.add_argument("--include_original", type=bool, default=True)
parser.add_argument("--bgs_for_img", type=list, default=8)
parser.add_argument("--use_rgb", type=bool, default=True)
parser.add_argument("--ck_low_lv", type=list, default=[0, 0, 123])
parser.add_argument("--ck_high_lv", type=list, default=[89, 255, 255])
args = parser.parse_args()

ckdata = CKDataset(
    augment_background=args.augment_background,
    augment_foreground=args.augment_foreground,
    use_rgb=args.use_rgb,
    output_shape=args.output_shape,
    background_cuts_for_image=args.bgs_for_img,
    debug=args.debug
)

ckdata.create(
    background_folder=args.backgroundfolder,
    input_images_folder=args.inputfolder,
    output_folder=args.outputfolder,
    include_original=args.include_original,
    lastindex=args.lastindex,
    clean_folders=args.clean_folders,
    ck_low_lv=args.ck_low_lv,
    ck_high_lv=args.ck_high_lv
)
