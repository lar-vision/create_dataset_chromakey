import cv2
import numpy as np
import glob
import os
import shutil
import random
import argparse
from lib.ck_bg_replace import Chromakey
from albumentations import Compose, Flip, HueSaturationValue, ChannelShuffle, Resize, ShiftScaleRotate, ElasticTransform, MotionBlur, ToGray, RandomCrop, RandomScale


class CKDataset(object):

    BGAUGMENT = Compose([
        Flip(p=0.5),
        MotionBlur(blur_limit=(50, 70), p=0.1),
        ElasticTransform(alpha=5000, sigma=60, alpha_affine=0, interpolation=3, border_mode=2, p=0.2),
        ShiftScaleRotate(shift_limit=0.0625, scale_limit=0.1, rotate_limit=90, p=1),
        RandomScale(scale_limit=(-0.3, 0), interpolation=3, p=0.8)  # make sure that the bg is still >= that the image size after rescaling
    ], p=1)

    BGAUGMENT_FT = Compose([   # 4 FINE-TUNING
        Flip(p=0.5),
        RandomScale(scale_limit=(-0.5, -0.1), interpolation=3, p=1)
    ], p=1)

    FGAUGMENT = Compose([
        HueSaturationValue(hue_shift_limit=-100, sat_shift_limit=0, val_shift_limit=0, p=0.5),
        ChannelShuffle(p=0.5),
        ToGray(p=0.1)
    ], p=0.5)

    def __init__(self, augment_background=False, augment_foreground=False, augmented_output=False, use_rgb=False, output_shape=(360, 640), background_cuts_for_image=1, dataset_fine_tuning=False, debug=False):
        self.lastindex = -1
        self.use_rgb = use_rgb
        self.debug = debug
        self.augment_background = augment_background
        self.augment_foreground = augment_foreground
        self.dataset_fine_tuning = dataset_fine_tuning
        self.augmented_output = augmented_output
        self.outheight = output_shape[0]
        self.outwidth = output_shape[1]

        self.background_cuts_for_image = background_cuts_for_image

    def adjust_images(self, image, mask, resize=False):
        outimage = image.copy()
        outmask = mask.copy()

        # scale image
        if self.outwidth > 0 and self.outheight > 0:
            outimage = cv2.resize(outimage, (self.outwidth, self.outheight), interpolation=cv2.INTER_AREA)
            outmask = cv2.resize(outmask, (self.outwidth, self.outheight), interpolation=cv2.INTER_AREA)

        # convert to grey scale
        if not self.use_rgb:
            outimage = cv2.cvtColor(outimage, cv2.COLOR_RGB2GRAY)
            # outimage = np.stack((outimage,) * 3, axis=-1)  # MAKE 3 LAYERS OF GREYSCALE

        return outimage, outmask

    def create(self, background_folder, input_images_folder, output_folder,  clean_folders=True, lastindex=None, include_original=True, ck_low_lv=None, ck_high_lv=None):

        self.clean_folders = clean_folders

        if lastindex is not None:
            self.lastindex = lastindex

        foreground_images = glob.glob(os.path.join(input_images_folder, "*.*"))
        background_images = glob.glob(os.path.join(background_folder, "*.*"))

        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        output_images_folder = os.path.join(output_folder, "imgs")
        if self.clean_folders:
            if os.path.exists(output_images_folder):
                shutil.rmtree(output_images_folder)
            os.makedirs(output_images_folder)

        output_masks_folder = os.path.join(output_folder, "masks")
        if self.clean_folders:
            if os.path.exists(output_masks_folder):
                shutil.rmtree(output_masks_folder)
            os.makedirs(output_masks_folder)

        ####################################################################
        # find CK levels (if not known) and/or check the output (if debug mode or levels previusly unknown)

        done = False
        while not done:
            image = cv2.imread(random.choice(foreground_images))
            ck = Chromakey(image, lower_green=ck_low_lv, upper_green=ck_high_lv)

            while self.debug or ck_high_lv is None or ck_low_lv is None:
                print("check")
                image = cv2.imread(random.choice(foreground_images))
                outmask, outimage = ck.bgreplace(fgimage=image, bgimage=np.ones(image.shape) * 255)
                cv2.imshow('outmask', outmask)
                cv2.imshow('outimage', outimage)
                key = cv2.waitKey(0) & 0xFF
                if key == ord("s"):
                    cv2.destroyAllWindows()
                    print("LEVELS: {}/{}".format(ck.lower_green, ck.upper_green))
                    done = True
                    break
                if key == ord("q"):
                    cv2.destroyAllWindows()
                    exit()
                if key == ord("m"):
                    cv2.destroyAllWindows()
                    break

            if not self.debug:
                done = True

        ####################################################################
        # cv2.namedWindow("img", cv2.WINDOW_NORMAL)

        index = self.lastindex + 1

        for image_file in sorted(foreground_images):

            image = cv2.imread(image_file)
            outmask, _ = ck.ckmask(fgimage=image.copy())

            ##################### INCLUDE ORIGINAL #########################
            if include_original:

                outimage = image.copy()

                # adjust images
                outimage, outmask = self.adjust_images(outimage, outmask)

                # save
                print("img={} (original)".format(index))
                output_image_path = os.path.join(output_images_folder, "{}.png".format(index))
                output_mask_path = os.path.join(output_masks_folder, "{}.png".format(index))

                cv2.imwrite(output_image_path, outimage)
                cv2.imwrite(output_mask_path, outmask)
                index = index + 1

            ##################### CHANGE BACKGROUND #########################
            # background
            background_file = random.choice(background_images)
            background = cv2.imread(background_file)
            hi, wi, _ = image.shape
            hb, wb, _ = background.shape

            for n in range(self.background_cuts_for_image):

                fg = image.copy()
                bg = background.copy()

                # background image augmentation
                if self.augment_background:
                    data = {"image": bg}
                    if self.dataset_fine_tuning:
                        bg = CKDataset.BGAUGMENT_FT(**data)["image"]
                    else:
                        bg = CKDataset.BGAUGMENT(**data)["image"]

                # Random Crop of the background with the fg-size
                bgcrop = Compose([RandomCrop(hi, wi, always_apply=True)], p=1)
                data = {"image": bg}
                bg = bgcrop(**data)["image"]

                # foreground image augmentation
                if self.augment_foreground:
                    data = {"image": fg}
                    fg = CKDataset.FGAUGMENT(**data)["image"]

                # REPLACE BACKGROUND
                outmask, outimage = ck.bgreplace_bymask(fgimage=fg, bgimage=bg, fgmask=outmask)

                # adjust images
                outimage, outmask = self.adjust_images(outimage, outmask)

                # save
                print("img={} ".format(index))
                output_image_path = os.path.join(output_images_folder, "{}.png".format(index))
                output_mask_path = os.path.join(output_masks_folder, "{}.png".format(index))

                cv2.imwrite(output_image_path, outimage)
                cv2.imwrite(output_mask_path, outmask)
                index = index + 1

        cv2.destroyAllWindows()
        self.lastindex = index
