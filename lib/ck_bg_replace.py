import cv2
import numpy as np
import glob
import os
import argparse


class Chromakey(object):

    def __init__(self, image, debug=False, lower_green=None, upper_green=None):

        self.borderpx = 1
        self.debug = debug

        if lower_green is not None and upper_green is not None:
            self.lower_green = np.array(lower_green)
            self.upper_green = np.array(upper_green)

        else:
            panel = np.zeros([100, 700], np.uint8)
            cv2.namedWindow('panel')

            def nothing(x):
                pass

            cv2.createTrackbar('L - h', 'panel', 0, 179, nothing)
            cv2.createTrackbar('U - h', 'panel', 179, 179, nothing)

            cv2.createTrackbar('L - s', 'panel', 0, 255, nothing)
            cv2.createTrackbar('U - s', 'panel', 255, 255, nothing)

            cv2.createTrackbar('L - v', 'panel', 0, 255, nothing)
            cv2.createTrackbar('U - v', 'panel', 255, 255, nothing)

            cv2.createTrackbar('S ROWS', 'panel', 0, 1080, nothing)
            cv2.createTrackbar('E ROWS', 'panel', 1080, 1080, nothing)
            cv2.createTrackbar('S COL', 'panel', 0, 1920, nothing)
            cv2.createTrackbar('E COL', 'panel', 1920, 1920, nothing)

            while True:
                frame = np.array(image)

                s_r = cv2.getTrackbarPos('S ROWS', 'panel')
                e_r = cv2.getTrackbarPos('E ROWS', 'panel')
                s_c = cv2.getTrackbarPos('S COL', 'panel')
                e_c = cv2.getTrackbarPos('E COL', 'panel')

                roi = frame[s_r: e_r, s_c: e_c]

                hsv = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)

                l_h = cv2.getTrackbarPos('L - h', 'panel')
                u_h = cv2.getTrackbarPos('U - h', 'panel')
                l_s = cv2.getTrackbarPos('L - s', 'panel')
                u_s = cv2.getTrackbarPos('U - s', 'panel')
                l_v = cv2.getTrackbarPos('L - v', 'panel')
                u_v = cv2.getTrackbarPos('U - v', 'panel')

                self.lower_green = np.array([l_h, l_s, l_v])
                self.upper_green = np.array([u_h, u_s, u_v])

                # print(self.lower_green, self.upper_green)

                bgmask = cv2.inRange(hsv, self.lower_green, self.upper_green)
                # bgmask = cv2.morphologyEx(bgmask,cv2.MORPH_GRADIENT,np.ones((5,5))) ## opening
                bgmask = cv2.morphologyEx(bgmask, cv2.MORPH_CLOSE, np.ones((5, 5)))  # closing
                # bgmask = cv2.GaussianBlur(bgmask, (3, 3), 0)
                # bgmask[bgmask <= 0.5] = [0]
                # bgmask[bgmask > 0.5] = [1]
                fgmask = cv2.bitwise_not(bgmask)

                bg = cv2.bitwise_and(roi, roi, mask=fgmask)
                fg = cv2.bitwise_and(roi, roi, mask=bgmask)

                bgmask = np.array(bgmask)
                bgmask[bgmask != 0] = [0]

                cv2.namedWindow("bgmask", cv2.WINDOW_NORMAL)
                cv2.namedWindow("bg", cv2.WINDOW_NORMAL)
                cv2.namedWindow("fg", cv2.WINDOW_NORMAL)
                cv2.imshow('bgmask', bgmask)
                cv2.imshow('bg', bg)
                cv2.imshow('fg', fg)
                cv2.imshow('panel', panel)

                key = cv2.waitKey(1) & 0xFF
                if key == ord("q"):
                    cv2.destroyAllWindows()
                    break

    def bgreplace(self, fgimage, bgimage):
        bgimage = bgimage[0:fgimage.shape[0], 0:fgimage.shape[1]]

        fgmask, bgmask = self.ckmask(fgimage)
        fgmask, outimg = self.bgreplace_bymask(fgimage, bgimage, fgmask)

        return fgmask, outimg

    def ckmask(self, fgimage):
        fgimage = np.array(fgimage)
        hsv = cv2.cvtColor(fgimage, cv2.COLOR_BGR2HSV)
        bgmask = cv2.inRange(hsv, self.lower_green, self.upper_green)
        bgmask = cv2.morphologyEx(bgmask, cv2.MORPH_CLOSE, np.ones((5, 5)))  # closing
        # bgmask = cv2.morphologyEx(bgmask, cv2.MORPH_DILATE, np.ones((self.borderpx, self.borderpx)))
        fgmask = cv2.bitwise_not(bgmask)
        return fgmask, bgmask

    def bgreplace_bymask(self, fgimage, bgimage, fgmask, filterksize=17):
        bgimage = bgimage[0:fgimage.shape[0], 0:fgimage.shape[1]]

        # we perform a linear combination
        # outimg = A*foreground + B*background
        # where A = "blured mask", B = 1-A
        bgmask = cv2.bitwise_not(fgmask)

        # bluring the mask (normalized) with a gaussian filer
        mask_bg_filt = bgmask.astype(float) / 255
        mask_bg_filt = cv2.GaussianBlur(mask_bg_filt, (filterksize, filterksize), 0)
        mask_bg_filt = np.stack((mask_bg_filt,) * 3, axis=-1)
        mask_fg_filt = 1.0 - mask_bg_filt

        bgimage = bgimage.astype(float) / 255
        fgimage = fgimage.astype(float) / 255

        # linear combination
        outbgimage = np.multiply(mask_bg_filt, bgimage)
        outfgimage = np.multiply(mask_fg_filt, fgimage)
        outimg = outbgimage + outfgimage

        bgimage = np.array(bgimage * 255).astype(np.uint8)
        fgimage = np.array(fgimage * 255).astype(np.uint8)
        outimg = np.array(outimg * 255).astype(np.uint8)

        if self.debug:
            cv2.imshow('fgimage', outfgimage)
            cv2.imshow('bgimage', outbgimage)
            cv2.imshow('fgmask', fgmask)
            cv2.imshow('bgmask', bgmask)
            cv2.imshow('outimg', outimg)
            key = cv2.waitKey(0)

        return fgmask, outimg


if __name__ == '__main__':
    image = cv2.imread("/media/riccardo/data1/datasets/dlo_segmentation/data_chromakey/ck_raw/b/frame353.png")
    ck = Chromakey(image, debug=True)
    # ck.lower_green = np.array([32, 0, 0])
    # ck.upper_green = np.array([255, 255, 255])
    print(ck.lower_green, ck.upper_green)

    background = cv2.imread("/media/riccardo/data1/datasets/dlo_segmentation/data_chromakey/bg/rainbow_cubes2.jpg")
    fgmask, outimg = ck.bgreplace(fgimage=image, bgimage=background)
    cv2.imshow('outimg', outimg)
    key = cv2.waitKey(0) & 0xFF
