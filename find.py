
import argparse
import random
import glob
import os
import cv2
import numpy as np
from lib.create_dataset import Chromakey

''' Find the HSV levels of the images contained in @inputfolder. '''

parser = argparse.ArgumentParser()
parser.add_argument("--inputfolder", type=str, default="/media/riccardo/data1/datasets/dlo_segmentation/data_chromakey/ck_raw/CK_OBJECTS/torodlr")
parser.add_argument("--output_shape", type=tuple, default=(-1, -1))
parser.add_argument("--ck_low_lv", type=list, default=None)
parser.add_argument("--ck_high_lv", type=list, default=None)
args = parser.parse_args()

foreground_images = glob.glob(os.path.join(args.inputfolder, "*.*"))
done = False
while not done:
    random_image_path = random.choice(foreground_images)
    print(random_image_path)
    image = cv2.imread(random_image_path)
    ck = Chromakey(image, lower_green=args.ck_low_lv, upper_green=args.ck_high_lv)

    while args.ck_low_lv is None or args.ck_high_lv is None:
        image = cv2.imread(random.choice(foreground_images))
        outmask, outimage = ck.bgreplace(fgimage=image, bgimage=np.ones(image.shape) * 255)
        outimage = cv2.resize(outimage, (640, 360), interpolation=cv2.INTER_AREA)
        outmask = cv2.resize(outmask, (640, 360), interpolation=cv2.INTER_AREA)
        cv2.imshow('outmask', outmask)
        cv2.imshow('outimage', outimage)

        key = cv2.waitKey(0) & 0xFF
        if key == ord("m"):
            cv2.destroyAllWindows()
            break
        if key == ord("s"):
            cv2.destroyAllWindows()
            print("LEVELS: {}/{}".format(ck.lower_green, ck.upper_green))
            exit()
