# Raw Dataset
images with green screen 
https://liveunibo-my.sharepoint.com/:f:/g/personal/riccardo_zanella2_unibo_it/EhMyGeEWnYtCvzJo8I3NhMwBukv1VPlRU_ArSZ1pxmvnig?e=yVmYNV

# Backgrounds 
https://liveunibo-my.sharepoint.com/:f:/g/personal/riccardo_zanella2_unibo_it/EvWyY7nFI75Or4btHXb_q3sBvq9P--uuPqBdLItvs6RpQw?e=DY4x3u

# Usage 

If we don't know the CK levels for the config file run "find.py" giving the input images folder path. 
A pannel will appear. Select the correct HSV levels using the sliding bars for the sample images. When you are done, press "q".
The result will be shown in new windows. Press "Enter" to see change sample image. If you need to change the HSV levels press "m", otehrwise press "s" for end.

#### 1) modify the "*.json" file in /config folder with the HSV levels and all the other parameters
#### 2) run create_full_dataset.py providing the path to the configuration file

 



