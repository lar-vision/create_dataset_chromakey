import os
import glob
import argparse
import json
from lib.ck_bg_replace import Chromakey
from lib.create_dataset import CKDataset

''' Create a dataset, ready for training. Input image folders, backgrounds, output folder, HSV levels, 
and all the other paramters can be set in a configuration file @config.'''

parser = argparse.ArgumentParser()
parser.add_argument("--config", type=str, default="config/config_create_dataset.json")
args = parser.parse_args()

config_file = args.config
with open(config_file) as json_file:
    config_dict = json.load(json_file)

num_input_imgs = 0
for itemID in config_dict["info"].keys():
    fg_imgs = glob.glob(os.path.join(config_dict["info"][itemID]["inputfolder"], "*.*"))
    num_input_imgs += len(fg_imgs)

num_tot_imgs = num_input_imgs * (config_dict["bgs_for_img"] + 1)

print("@@@@@@@@@@@@@@ {} images".format(num_tot_imgs))

######################################################################

ckdata = CKDataset(
    augment_background=config_dict["augment_background"],
    augment_foreground=config_dict["augment_foreground"],
    dataset_fine_tuning=config_dict["dataset_fine_tuning"],
    use_rgb=config_dict["use_rgb"],
    output_shape=config_dict["output_shape"],
    background_cuts_for_image=config_dict["bgs_for_img"],
    debug=config_dict["debug"]
)

######################################################################
######################################################################
######################################################################

first = True

for itemID in config_dict["info"].keys():
    print("@" * 20, itemID)

    ckdata.create(
        background_folder=config_dict["backgroundfolder"],
        input_images_folder=config_dict["info"][itemID]["inputfolder"],
        output_folder=config_dict["outputfolder"],
        lastindex=config_dict["lastindex"] if first else None,
        clean_folders=True if first else False,
        include_original=config_dict["include_original"],
        ck_low_lv=config_dict["info"][itemID]["ck_low_lv"],
        ck_high_lv=config_dict["info"][itemID]["ck_high_lv"]
    )

    print("@@@@@@@@@@@@@@ {}".format(100.0 * ckdata.lastindex / num_tot_imgs))

    if first:
        first = False
