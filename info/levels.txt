

NB dataset:
* blue1: 
    ck_low_lv = [0,0,0]
    ck_high_lv = [99,255, 255] 
* yellow
    ck_low_lv = [35,0,0]
    ck_high_lv = [179,255, 255]
* orange
    ck_low_lv = [15,0,0]
    ck_high_lv = [145,255, 255]



NB dataset:
* b: 
    ck_low_lv = [0,0,0]
    ck_high_lv = [90,255, 255]
* y
    ck_low_lv = [40,0,0]
    ck_high_lv = [179,255, 255]
* r
    ck_low_lv = [50,0,0]
    ck_high_lv = [150,255, 255]
* k
    ck_low_lv = [0,110,100]
    ck_high_lv = [179,255, 255]
* w
    ck_low_lv = [35,60,100]
    ck_high_lv = [179,255, 255]

* bk
    ck_low_lv = [0,110,100]
    ck_high_lv = [90,255, 255]
* wk
    ck_low_lv = [50,60,70]
    ck_high_lv = [179,255, 255]
* yb
    ck_low_lv = [50,0,0]
    ck_high_lv = [90,255, 255]
* yw
    ck_low_lv = [50,60,0]
    ck_high_lv = [179,255, 255]
